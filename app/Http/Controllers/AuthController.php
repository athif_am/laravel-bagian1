<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form (){
        return view ('form');
    }

    public function welcome (request $register){
        $first = $register["firstname"];
        $last = $register["lastname"];

        return  "Selamat datang $first $last ! <br><br> Terima Kasih telah bergabung dengan Sanberbook";
    }
}
