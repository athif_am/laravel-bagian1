<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sanberbook</title>
</head>
<body>
    <form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <label>First Name</label><br />
        <input type="text" name="firstname"/><br />
        <label>Last Name</label><br />
        <input type="text" name="lastname"/><br />
        <label>Gender</label><br />
        <input type="radio" name="gender"/>Male<br />
        <input type="radio" name="gender"/>Female<br />
        <input type="radio" name="gender"/>Other<br />
        <input type="text"/><br />
        <label>Nationality</label><br />
        <select>
            <option> Indonesian</option>
            <option> Malaysia</option>
            <option> Siongapore</option>
            <option> Australian</option>
        </select>
        <br />
        <label>Language Spoken</label></label><br />
        <input type="checkbox"/> Bahasa Indonesia<br />
        <input type="checkbox"/> English<br />
        <input type="checkbox"/> Other<br />
        <label>Bio</label><br />
        <textarea cols="20" rows="10"></textarea><br />
        <br />
        <input type="submit"/>
    </form>
</body>

</html>